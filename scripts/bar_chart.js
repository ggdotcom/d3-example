var margin = {top: 20, right: 20, bottom: 30, left: 40};
var width = 960 - margin.left - margin.right;
var height = 500 - margin.top - margin.bottom;

var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], 0.1);

var y = d3.scale.linear()
    .range([height, 0]);

// Scale for opacities
var o = d3.scale.sqrt()
    .range([0, 1]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient('bottom');

var yAxis = d3.svg.axis()
    .scale(y)
    .orient('left')
    .ticks(10, '%');

var chart = d3.select('#chart')
    .attr('width', width + margin.left + margin.right)
    .attr('height', height + margin.top + margin.bottom)
  .append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

// Get data from file
d3.tsv('data/data.tsv', function(d) {
  d.frequency = +d.frequency;
  return d;
}, function(error, data) {
  x.domain(data.map(function(d) { return d.letter; }));

  // Append x axis
  chart.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0,' + height + ')')
      .call(xAxis);

  // Append y axis
  chart.append('g')
      .attr('class', 'y axis')
      .call(yAxis)
    .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 6)
      .attr('dy', '.71em')
      .style('text-anchor', 'end')
      .text('Frequency');

  redraw(data);
  dataUpdate(data);
});

// This function mimics updates on the underlying data (it could be replaced by
// an update on the data that comes from some server)
function dataUpdate(data) {
  setTimeout(function() {
    var newData = data.map(function(d) {
      return {
        letter: d.letter,
        frequency: d.frequency * Math.random() * 2,
      };
    });
    redraw(newData);
    dataUpdate(data);
  }, Math.random() * 5000);
}

// Updates data on DOM
function redraw(data) {
  var maxFreq = d3.max(data, function(d) { return d.frequency; });
  y.domain([0, maxFreq]);
  o.domain([0, maxFreq]);

  // Update y axis
  chart.select('.y.axis').transition().call(yAxis);

  // Updates bars with new data
  var bars = chart.selectAll('.bar')
      .data(data, function(d) { return d.letter; });

  // Enter
  bars.enter().append('rect')
    .attr('class', 'bar')
    .attr('x', function(d) { return x(d.letter); })
    .attr('width', x.rangeBand())
    .attr('y', y(0))
    .attr('height', 0);

  // Enter and update
  bars.transition()
    .attr('y', function(d) { return y(d.frequency); })
    .attr('height', function(d) { return height - y(d.frequency); })
    .style('opacity', function(d) { return o(d.frequency); });
}
